# Typescript Jest Boilerplate

This is a boilerplate for a Typescript project with Jest for testing.

## Getting Started

1. Clone the repository
2. Run `npm install`
3. Run `npm run test` to run the tests
4. Run `npm run build` to build the project
5. Run `npm run start` to run the project
